export function fetchJ(req) {
    return new Promise((resolve) => {
        fetch(req)
            .then((r) => { return r.json()})
            .then((j) => resolve(j))
    })
}