import { createReq } from '../../src/createReq';
import { SERVER_NAME } from '../../src/config';
import { fetchJ } from './fetchJ';

export function getReqData(q) {
    let rO = q;
    let rS = createReq(rO);
    let req = `..\\${ SERVER_NAME }?${ rS }`;
    
    return new  Promise((resolve) => fetchJ(req)
        .then((j) => {
            resolve(j);
        })
    );
}