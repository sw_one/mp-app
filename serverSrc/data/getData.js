import { app } from '../index';
import { getJsonData } from './getJsonData';
import { getReqData } from './getReqData';

const jsonData = {
    'classlist' : `..\\data\\classes.json`,
    'terrains' : '../data/terrains.json',
};

const reqData = {
    'units' : { q: 'echo' },
    'map': { q: 'getmap' },
};

export function getMapData() {
    getReqData(reqData.map)
        .then(j => {
            app.dim = { x: j.x, y: j.y };
            app.map = j.data;
        });
}

export function getGenMap() {
    let r = { q: 'mapgen', dimx: app.dim.x, dimy: app.dim.y };
    getReqData(r)
        .then(j => {
            app.map = j.data;
        })
}

export function getData() {

    getReqData(reqData.units)
        .then(j => app.units = j.units);

    getMapData();

    Object.keys(jsonData).forEach((entry) => {
        getJsonData(jsonData[entry]).then(j => app[entry] = j);
    })

}