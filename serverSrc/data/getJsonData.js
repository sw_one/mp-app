import { fetchJ } from './fetchJ';

export function getJsonData(req) {
    return new Promise((resolve) => fetchJ(req)
        .then((j) => {
            //console.log(j);
            resolve(j);
        })
    );
}