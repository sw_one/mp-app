import { sendRequest } from './sendRequest';

export function saveMapData(x, y, map) {
    let r = { q: 'savemap', dimx: x, dimy: y, data: JSON.stringify(map) };
    sendRequest(r, 'saveMapData');
}

export function saveUnitsData(data) {
    let r = { q: 'save', data: JSON.stringify(data) };
    sendRequest(r, 'saveUnitsData');
}

//action
export function setZeroTurn() {
    let r = { q: 'setzt' };
    sendRequest(r, 'setZeroTurn');
}