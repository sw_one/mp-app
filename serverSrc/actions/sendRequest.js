import {SERVER_NAME} from '../../src/config';
import {createReq} from '../../src/createReq';

export function sendRequest(r, name = '') {
    let confirm = window.confirm('Save data?');
    if(!confirm) return;

    let rS = createReq(r);
    let req = `..\\${ SERVER_NAME }?${ rS }`;

    return new Promise((resolve) => fetch(req)
        .then((req) => {
            if(req.ok) {
                console.log(name, 'request sucess!');
            } else {
                throw Error(req.statusText);
            }
        })
        .catch((err) => { console.warn(`${ name } error ${ err }`); })
    );
}