const eir = 5;

export function splitObject(dset) {
    let keyz = Object.keys(dset);
    let l = keyz.length;
    let lr =  Math.ceil(l / eir);
    let splitted = {};

    for (let i = 0; i < lr; i++) {
        splitted[i] = {};
        for(let j = 0; j < eir; j++) {
            let index = i * eir + j;
            if(index < l) {
                let id = keyz[index];
                let e = dset[id];
                //console.log(dset, index, e);
                splitted[i][id] = e;
            }
        }
    }

    return splitted;
}