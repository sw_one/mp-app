import Vue from 'vue';
import Manager from './views/UnitManager';
import MapEditor from './views/MapEditor';

import { getData } from './data/getData';

export const app = new Vue({
    el: '#app',
    data: {
        dim: { x: null, y: null },
        map: null,
        units: {},
        classlist: {},
        tab: 'units',
        terrains: {}
    },
    template:
    `
    <div>
        Server here<br>
        <button @click="clickTab($event)" data-tab="units">Units</button> |
        <button @click="clickTab($event)" data-tab="map">Map</button>
        <hr>
        <br>
        <template v-if="activeTab('units')">
        {{ units }}<br>
        <Manager 
            :udata="units"
            :classes="classlist"
        />
        </template>
        <template v-if="activeTab('map')">
            <MapEditor 
                :map="map"
                :dim="dim"
                :units="playersF()"
                :terr-list="terrains"
            />
        </template>
    </div>
    `,
    components: {
        'Manager': Manager,
        'MapEditor': MapEditor
    },
    methods: {
        playersF() {
            let d = {};
            let p = this.units;
            
            Object.keys(p).forEach((i) =>{

                let id = i;
                let x = p[i].x;
                let y = p[i].y;
                let z = x + this.dim.x * y;

                d[z] = { id, x, y, data: p[i].data };
            });

            return d;
        },
        activeTab: function(tab) {
            return tab === this.tab;
        },
        clickTab: function(ev) {
            let tab = ev.target.dataset.tab;
            console.log(tab);
            this.tab = tab;
            localStorage.tab = tab;
        }
    }
});

window.onload = () => {
    getData();
    if(localStorage.tab) {
        let l = localStorage.tab;
        //console.log(l);
        app.tab = localStorage.tab;
    }
    /* window.addEventListener('click', (ev) => { 
        console.log(ev);
    }); */
    window.addEventListener('dragstart', (ev) => {
        let _e = ev.target.id;
        console.log(_e);
    });
};