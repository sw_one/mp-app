import { app } from './index';
import { SERVER_NAME, MIN_LOCK_DELAY } from './config';
import { createReq } from './createReq';

export function sendNewPosition(x, y) {
    return new Promise((resolve, reject) => {
        app.lock = true;

        let rO = {
            q: 'turn', x, y, pid: app.player
        };
        let rS = createReq(rO);

        let req = `${ SERVER_NAME }?${ rS }`;

        fetch(req)
            .then((res) => {
                if (res.ok) {
                    return res.json();
                } else {
                    throw new Error('fetch failed');
                }
            })
            .then((res) => {
                window.setTimeout(() => {
                    app.lock = false;
                    resolve(res);
                }, MIN_LOCK_DELAY);
            })
            .catch((err) => {
                app.lock = false;
                reject(err);
            });
    });
}