import Vue from 'vue/dist/vue.js';
import { MAP } from './MAP';
import { setStartData } from './setStartData';
import { KeyEvents } from './KeyEvents/KeyEvents';
import { dimX, dimY, PING } from './config';
import { updateOnPing } from './updateOnPing';

import Test from './views/Test';
import Controls from './views/Controls';
import Board from './views/Board';
import PlayerSelector from './views/PlayerSelector';
import TextMap from './views/TextMap'

const app = new Vue({
    el: '#app',
    data: function() {
        return {
            turn: null,
            player: 'id1',//hardcode
            map: MAP,
            x: null, //startX,
            y: null, //startY,
            lock: false,
            gameState: null,
            players: {}
        };
    },
    template:
        `<div>
            <!-- <Test /> -->
            Turn: {{ turn }}<br>
            <Controls 
                :lock="lock"
                :gameState="gameState"
            />
            <PlayerSelector
                :current="player"
                :players="players"
            />
            <Board
                :map="map"
                :x="x"
                :y="y"
                :players="playersF()"
            />
            <TextMap :map="map" />
        </div>`
    ,
    methods: {
        playersF() {
            let d = {};
            let p = this.players;
            
            Object.keys(p).forEach((i) =>{

                let id = i;
                let x = p[i].x;
                let y = p[i].y;
                let z = x + dimX * y;
        
                d[z] = { id, x, y };
            })

            return d;
        }
    },
    components: {
        'Test': Test,
        'Controls': Controls,
        'Board': Board,
        'PlayerSelector': PlayerSelector,
        'TextMap': TextMap
    }
});

let keyEvents = new KeyEvents();

window.onload = function () {
    document.addEventListener('click', (ev) => {
        let id = ev.target.id;
        if(id) console.log(ev.target.id);
    });
    keyEvents.init();
    setStartData();
    window.setInterval(updateOnPing, PING);
};

export { app };