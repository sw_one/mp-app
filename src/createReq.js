export function createReq(data) {
    let req = '';
    Object.keys(data).forEach((key, i) => {
        let _amp = (i === 0) ? '' : '&';
        req += `${ _amp }${ key }=${ data[key] }`;
    });
    return req;
}