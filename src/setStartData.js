import { pingState } from './pingState';
import { setUnitsData } from './setUnitsData';
import { app } from './index';


export function setStartData() {
    //fetch('state.json').then((a) => { return a.json() })
    pingState()
        .then((j) => {
            console.log('get start state', j);
            let units = j.units;
            setUnitsData(units);

            app.turn = j.turn;
        });
}