import { dimX ,dimY } from './config';

export const MAP = [];

//const dimX = 10;
//const dimY = 10;
//const startX = 0;
//const startY = 0;

for (let y = 0; y < dimY; y++) {
    let row = [];
    for(let x = 0; x < dimX; x++) {
        row.push(0);
    }
    MAP.push(row);
}

//MAP[startY][startX] = MARKER;