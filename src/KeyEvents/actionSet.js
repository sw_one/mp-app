import { keys as k } from './keys';
import { move } from '../move';
import { pingState } from '../pingState';


export let actionSet = {
    'asd': () => { console.log('Test'); },
    
    [`${ k.u }`]() { move(0, -1); },
    [`${ k.ur }`]() { move(1, -1); },
    [`${ k.r }`]() { move(1, 0); },
    [`${ k.rd }`]() { move(1, 1); },
    [`${ k.d }`]() { move(0, 1); },
    [`${ k.dl }`]() { move(-1, 1); },
    [`${ k.l }`]() { move(-1, 0); },
    [`${ k.ul }`]() { move(-1, -1); },

    [`${ k.p }`]() { 
        console.log('ping!');
        pingState();
    }

};