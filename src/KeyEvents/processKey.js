import { app } from '../index';
import { actions } from './KeyActions';

export function prosessKey(k) {
    if(app.lock) return;
    if(actions[k]) { 
        //console.log('process', k);
        actions[k](); 
    }
}