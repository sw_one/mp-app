import { letters } from './kbd';
import { actionSet as c } from './actionSet';

export let actions = {};

function getOpCode(keys) {
    let _a = Array.from(keys);
    let _c = _a.map((c) => { return letters[c]; });
    let _o = _c.reduce((o, i) => { return o + i; });

    return _o;
}

function assignAction(keys, action) {
    let op = getOpCode(keys);
    actions[op] = action;
}

Object.keys(c).forEach((i) => assignAction(i, c[i]));

//assignAction('asd', () => console.log('Test'));
//console.log(actions);