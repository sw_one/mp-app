//import { app } from '../index';
import { prosessKey } from './processKey';
//import { actions } from './KeyActions';

export class KeyEvents {
    constructor() {
        this.keyArr = []
    }
    init() {
        document.addEventListener('keydown', (ev) => {
            //console.log(ev);
            let code = ev.keyCode;
            this.keyArr.push(code);
        });
        document.addEventListener('keyup', () => {
            if(this.keyArr.length > 0) {
    
                //console.log(':', this.keyArr);
                let opCode = this.keyArr.reduce((o, i) => { return o + i; });
                this.keyArr = [];
    
                //console.log('o->', opCode);
                prosessKey(opCode);
                //if(app.lock) return;
                //if(actions[opCode]) actions[opCode]();
            }
        });
    }
}