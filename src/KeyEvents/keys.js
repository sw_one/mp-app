export const keys = {
    up: 'KeyW',
    right: 'KeyD',
    down: 'KeyS',
    left: 'KeyA',

    //directions - wasd
    u: 'w',
    ur: 'wd',
    r: 'd',
    rd: 'ds',
    d: 's',
    dl: 'sa',
    l: 'a',
    ul: 'wa',

    p: 'p',
};