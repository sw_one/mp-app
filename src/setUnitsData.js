import Vue from 'vue/dist/vue';
import { app } from './index';
import { MARKER } from './config';
import { dimX, dimY } from './config';
import { MAP } from './MAP';

export function setUnitsData(data) {
    Vue.set(app, 'players', data);

    //clear local map
    for(let j = 0; j < dimY; j++) {
        for(let i = 0; i < dimX; i++) {
            Vue.set(app.map[j], i, 0);
        }
    }
    //update local map
    Object.keys(data).forEach((u) => {
        let id = u;
        let x = data[u].x;
        let y = data[u].y;
        //app.map[y][x] = MARKER;
        Vue.set(app.map[y], x, MARKER);
    });
}
