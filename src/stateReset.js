import { SERVER_NAME } from './config';
import { createReq } from './createReq';

let rO = { q: 'reset' };
let rS = createReq(rO);

let req = `${ SERVER_NAME }?${ rS }`;

export function stateReset() { fetch(req); }


