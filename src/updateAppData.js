import Vue from 'vue/dist/vue.js';
import { app } from './index.js';
import { MARKER } from './config';

export function updateAppData(x, y) {

    //console.log('uad', x, y);

    let cx = app.x;
    let cy = app.y;

    Vue.set(app.map[cy], cx, 0);
    Vue.set(app.map[y], x, MARKER);

    app.x = x;
    app.y = y;
}