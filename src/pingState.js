import { printState } from './printState';
import { SERVER_NAME } from './config';
import { createReq } from './createReq';

let rO = { q: 'echo' };
let rS = createReq(rO);
let req = `${ SERVER_NAME }?${ rS }`;

export function pingState() {
    return new Promise((resolve) => fetch(req) //'data/state.json'
        .then((a) => {
            return a.json();
        })
        .then((j) => {
            printState(j);
            //console.log(j);
            resolve(j);
        })
    );
}