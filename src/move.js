import { app } from './index';
import { dimX ,dimY } from './config';
import { sendNewPosition } from './sendNewPosition';
import { updateAppData } from './updateAppData';
import { printState } from './printState';
import { setUnitsData } from './setUnitsData';

export function move(dx, dy) {

    let id = app.player;

    let newX = app.players[id].x + dx;
    let newY = app.players[id].y + dy;

    if(( newX < 0 ) || (newX > dimX - 1) || (newY < 0) || (newY > dimY - 1)) return;

    sendNewPosition(newX, newY)
        .then((res) => {
            if(res.status ==='ok') {
                console.log('move to->', res);
                //updateAppData(res.x, res.y);
                printState(res.state);

                //let units = res.state.units;
                //console.log(units);
                //setUnitsData(units);
            }
        })
        .catch((err) => { console.warn('server error:', err); });
}