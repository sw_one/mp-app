import { SERVER_NAME } from './config';
import { createReq } from './createReq';

let rO = { q: 'base' };
let rS = createReq(rO);

let req = `${ SERVER_NAME }?${ rS }`;

export function stateFromDb() {
    fetch(req)
        .then((res) => {
            return res.text();
        })
        .then((a) => {
            console.log('fromdb->', a);
        });
}