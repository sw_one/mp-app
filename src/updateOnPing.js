import { app } from './index';
import { pingState } from './pingState';
import { setUnitsData } from './setUnitsData';

export function updateOnPing() {
    pingState().then((res) => {
        let turn = res.turn;
        let data = res.units;
        console.log(`update-> remote T ${ turn } local T ${ app.turn }`);
        //${ JSON.stringify(data) }

        if (turn > app.turn) {
            app.turn = turn;
            setUnitsData(data);
        }
    })
}