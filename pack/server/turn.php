<?php

if($q === 'turn') {

    $pid = $_GET['pid'];
    $x = (int)$_GET['x'];
    $y = (int)$_GET['y'];
    #$turn = (int)$_GET['turn'];

    //validate and write
    $state = json_decode(file_get_contents(FILE), true);

    //print_r($state);

    #$turn = (int)$_GET['turn'];
    $state['turn']++;

    $state['units'][$pid]['x'] = $x;
    $state['units'][$pid]['y'] = $y;

    //print_r($state);

    file_put_contents(FILE, json_encode($state));

    $answer = ['status' => "ok", 'id' => $pid, 'x' => $x, 'y' => $y, 'state' => $state];

    //echo "ok! -> move to ($x,$y)";
    echo json_encode($answer);
}
