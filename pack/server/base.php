<?php

if($q ==='base') {
    echo "php: fromdb->\n\r";

    $host = 'localhost';
    $database = 'test';
    $user = 'root';
    $password = '';

    $link = mysqli_connect($host, $user, $password, $database)
    or die('error ' . mysqli_error($link));

    echo  "connected!->\n\r";

    $query = "SELECT * FROM mpdata";
    $result = mysqli_query($link, $query)
    or die('error' . mysqli_error($link));

    if($result) {
        #print_r($result);
        $rows = mysqli_num_rows($result);
        for ($i = 0; $i < $rows; $i++) {
            $row = mysqli_fetch_row($result);

            #print_r($row);
            #$id = $row[0];
            #$data = $row[1];

            echo "[id] $row[0]: [data] $row[1]";
            echo "\n\r";
        }
        #echo "\n\rconnection ok!\n\r";
        mysqli_free_result($result);
    }

    mysqli_close($link);
}